import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import './App.css';

import Simple from './components/simple';
import Weight from './components/weight';

function App() {
  return (
    <div className="App">
      <header className="App-header p-2">
        <h2>Welcome To React JS Calculator</h2>
      </header>

      <Router>
        <div className="row">
          <div className="m-auto">
            <Link to="/"><button className="btn btn-outline-success m-3">Simple Calculator</button></Link>
            <Link to="/weight"><button className="btn btn-outline-success m-3">Weight Calculator</button></Link>
          </div>
        </div>

        <Switch>
          <Route path="/weight">
            <Weight />
          </Route>
          <Route exact path="/">
            <Simple />
          </Route>
        </Switch>
      </Router>

    </div>
  );
}
export default App;
