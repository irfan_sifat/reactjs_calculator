import React, { Component } from 'react';
import { create, all } from 'mathjs';
import ReactAnime from 'react-animejs';

const { Anime } = ReactAnime;

const config = {};
const math = create(all, config);

export default class Simple extends Component {

    state = {
        result: "0",
        firstN: "",
        secondN: "",
        sign: ""
    }

    equal = () => {
        var res;
        if (this.state.sign === '+') {
            res = Number(this.state.firstN) + Number(this.state.secondN);
        }
        if (this.state.sign === '-') {
            res = Number(this.state.firstN) - Number(this.state.secondN);
        }
        if (this.state.sign === '*') {
            res = Number(this.state.firstN) * Number(this.state.secondN);
        }
        if (this.state.sign === '/') {
            res = Number(this.state.firstN) / Number(this.state.secondN);
        }
        this.setState({
            result: math.round(res, 2),
            firstN: math.round(res, 2),
            secondN: ""
        })
    }

    cal = (e) => {
        var b_val = e.target.value;
        var res;
        // console.log(this.state);
        if (b_val === 'ac') {
            this.setState({
                result: "0",
                firstN: "",
                secondN: "",
                sign: ""
            })
        }

        if ((b_val === '%' && this.firstN !== "") || (b_val === '%' && this.state.result !== "0")) {
            res = Number(this.state.firstN) / 100;
            if (this.state.result !== "0") {
                res = Number(this.state.result) / 100;
            }
            this.setState({
                result: res,
                firstN: this.state.firstN,
                secondN: "",
                sign: ""
            })
        }

        if (b_val === '+' || b_val === '-' || b_val === '*' || b_val === '/') {
            this.setState({
                sign: b_val
            })
            if (this.state.sign === '-' && this.state.firstN !== "") {
                this.setState({
                    firstN: Number(0 - this.state.firstN)
                })
            }

            if (this.state.result !== "0") {
                this.setState({
                    firstN: math.round(this.state.result, 2),
                })
                // this.equal();
            }
            if (this.state.firstN !== "" && this.state.secondN !== "") {
                this.equal();
            }
        }


        if (b_val === '0' || b_val === '1' || b_val === '2' || b_val === '3' || b_val === '4' || b_val === '5' || b_val === '6' || b_val === '7' || b_val === '8' || b_val === '9' || b_val === '.') {
            if ((this.state.sign === "-" && this.state.firstN === "") || ((this.state.sign === "-" && this.state.firstN === ".")) || this.state.sign === "") {
                this.setState({
                    firstN: this.state.firstN + b_val,
                })
            } else {
                this.setState({
                    secondN: this.state.secondN + b_val,
                })
            }
        }

        if (b_val === '=') {
            if (this.state.firstN === "" || this.state.secondN === "") {

            } else {
                this.equal();
            }
        }
    }
    render() {
        var show;
        if ((this.state.firstN === "" && this.state.secondN === "") || (this.state.result !== "0" && this.state.firstN !== "")) {
            show = this.state.result;
            show = math.round(show, 2);
        } else if (this.state.secondN !== "" || (this.state.result !== "0" && this.state.firstN !== "")) {
            show = this.state.secondN;
        } else {
            show = this.state.firstN;
        }
        return (
            <div>

                <h3 className="ml16">Simple Calculator</h3>
                {/* <button className="badge btn btn-danger">Press <b>AC</b> for complete new calculation</button> */}

                <Anime
                    initial={[
                        {
                            targets: "#Box",
                            translateY: -200,
                            easing: "easeInOutSine",
                            opacity: 0,
                            duration: 10
                        },
                        {
                            targets: "#Box",
                            translateY: -20,
                            easing: "easeInOutSine",
                            opacity: 1,
                            duration: 1000
                        },
                    ]}
                >
                    <div id="Box" className="simpleCal">
                        <div className="simpleResult">
                            {show}
                        </div>
                        <div className="row">
                            <button value="ac" onClick={this.cal} className="btn btn-circle col-2">AC</button>
                            <button value="%" onClick={this.cal} className="btn btn-circle col-2">%</button>
                            <button value="/" onClick={this.cal} className="btn btn-circle col-2">/</button>
                            <button value="*" onClick={this.cal} className="btn btn-circle col-2">*</button>
                        </div>
                        <div className="row">
                            <button value="7" onClick={this.cal} className="btn btn-circle col-2">7</button>
                            <button value="8" onClick={this.cal} className="btn btn-circle col-2">8</button>
                            <button value="9" onClick={this.cal} className="btn btn-circle col-2">9</button>
                            <button value="-" onClick={this.cal} className="btn btn-circle col-2">-</button>
                        </div>
                        <div className="row">
                            <button value="4" onClick={this.cal} className="btn btn-circle col-2">4</button>
                            <button value="5" onClick={this.cal} className="btn btn-circle col-2">5</button>
                            <button value="6" onClick={this.cal} className="btn btn-circle col-2">6</button>
                            <button value="+" onClick={this.cal} className="btn btn-circle col-2">+</button>
                        </div>
                        <div className="row">
                            <button value="1" onClick={this.cal} className="btn btn-circle col-2">1</button>
                            <button value="2" onClick={this.cal} className="btn btn-circle col-2">2</button>
                            <button value="3" onClick={this.cal} className="btn btn-circle col-2">3</button>
                            <button value="=" onClick={this.cal} className="btn btn-circle col-2">=</button>
                        </div>
                        <div className="row">
                            <button value="0" onClick={this.cal} className="btn btn-circle col-5">0</button>
                            <button value="." onClick={this.cal} className="btn btn-circle col-3">.</button>
                            {/* <button className="btn btn-circle col-2">=</button> */}
                        </div>
                    </div>
                </Anime>
            </div>
        )
    }
}
