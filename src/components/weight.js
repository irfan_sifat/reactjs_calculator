import React, { Component } from 'react';
import { create, all } from 'mathjs';
import ReactAnime from 'react-animejs';

const { Anime } = ReactAnime;

const config = {};
const math = create(all, config);


export default class Weight extends Component {
    state = {
        result: "",
        gender: "",
        height: "",
        weight: ""
    }

    reset = () => {
        this.setState({
            result: "",
            gender: "",
            height: "",
            weight: ""
        })
    }

    cal = () => {
        // console.log(this.state);
        var ibw
        if (this.state.height === '' || this.state.weight === '') {
            alert("Please insert Height & Weight");
        } else {
            if (this.state.gender === 'male') {
                // convert cm to m 
                let m = Number(this.state.height) * 0.01;
                ibw = 22 * math.square(m);

                this.setState({
                    result: math.round(ibw, 2)
                })

            } else if (this.state.gender === 'female') {
                // convert cm to m 
                let m = Number(this.state.height) * 0.01;
                ibw = 22 * math.square(m - 0.1);
                this.setState({
                    result: math.round(ibw, 2)
                })
            } else {
                alert("Please select a Gender");
            }
        }
    }

    render() {
        var com;
        var show = "Ideal Body Weight ";
        if (this.state.result !== '') {
            show = "IBW: " + this.state.result;
            if (this.state.result > this.state.weight) {
                com = math.round((Number(this.state.result) - Number(this.state.weight)), 2);
                com = "You are " + com + " kg underweight.";
            } else if (this.state.result < this.state.weight) {
                com = math.round((Number(this.state.weight) - Number(this.state.result)), 2);
                com = "You are " + com + " kg overweight.";
            } else {
                com = "You are Perfect.";
            }
        }

        return (
            <div>
                <h3 className="ml16">Human Weight Calculator</h3>

                <Anime
                    initial={[
                        {
                            targets: "#Box",
                            translateY: -200,
                            easing: "easeInOutSine",
                            opacity: 0,
                            duration: 10
                        },
                        {
                            targets: "#Box",
                            translateY: -20,
                            easing: "easeInOutSine",
                            opacity: 1,
                            duration: 1000
                        },
                    ]}
                >
                    <div id="Box" className="weightCal">
                        <div className="simpleResult">
                            {show}
                        </div>
                        <div className="row">
                            <label className="col-4 m-2">Gender</label>
                            <label className="col-3 m-2">Height (CM)</label>
                            <label className="col-3 m-2">Weight (KG)</label>
                        </div>
                        <div className="row">
                            <select onChange={(e) => this.setState({ gender: e.target.value })} className="form-control m-2 col-4">
                                <option value='' defaultValue>--Select Gender--</option>
                                <option value='male'>Male</option>
                                <option value='female'>Female</option>
                            </select>
                            <input onChange={(e) => this.setState({ height: e.target.value })} className="form-control m-2 col-3" type="number"></input>
                            <input onChange={(e) => this.setState({ weight: e.target.value })} className="form-control m-2 col-3" type="number"></input>
                        </div>
                        <br></br>
                        <div className="row">
                            <button onClick={this.cal} className="btn btn-lg btn-outline-light">Calculate</button>
                            <button onClick={this.reset} className="btn btn-lg btn-outline-warning ml-2 float-right">Reset</button>
                        </div>
                        <br></br>
                        <center>
                            <h5 className="display-5 text-danger">{com}</h5>
                        </center>
                    </div>
                </Anime>
            </div>
        )
    }
}
